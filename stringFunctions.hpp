/* * * * * * * * * * * * *
  stringFunctions.hpp - Common fucntions for strings and similar objects
 * * * * * * * * * * * * */
#ifndef STRINGFUNCTIONS_HPP
#define STRINGFUNCTIONS_HPP

#include <iostream>
#include <string>
#include "numberFunctions.hpp"
using namespace std;

namespace commonFunctions {
    // Returns the given string with std::tolower() run on each char
    inline string tolower(string s) {
        string x;
        for (size_t t = 0; t < s.size(); t++)
            x.push_back(std::tolower(s.at(t)));
        return x;
    }

    // Returns given string with std::toupper() run on each char
    inline string toupper(string s) {
        string x;
        for (size_t t = 0; t < s.size(); t++)
            x.push_back(std::toupper(s.at(t)));
        return x;
    }

    // Return a reversed version of the given string
    inline string reverse(string s) {
        string x;
        for (size_t t = s.size() - 1; t >= 0; t--)
            x.push_back(s.at(t));
        return x;
    }

    // Used to specify return type of to_string(bool)
    enum boolMode { 
        TRUE,    // 'True' or 'False' (default)
        YES,     // 'Yes' or 'No'
        ENABLED, // 'Enabled' or 'Disabled'
        DEFAULT = TRUE
    };

    // What case to_string(bool) will return
    enum boolCase {
        LOWER, // All lower case (default)
        FIRST, // First letter of string capitalized
        UPPER, // All upper case
        DEFAULT = LOWER
    };

    // Returns string form of bool with given boolMode and boolCase
    inline string to_string(bool b, boolMode bm = boolMode::DEFAULT, boolCase bc = boolCase::DEFAULT) {
        string t, f;
        switch(bm) {
            case TRUE:
                t = "true";
                f = "false";
                break;
            case YES:
                t = "yes";
                f = "no";
                break;
            case ENABLED:
                t = "enabled";
                f = "disabled";
                break;
            // Hopefully not having a default case is OK
        }

        switch(bc) {
            case FIRST:
                t.at(0) = std::toupper(t.at(0));
                f.at(0) = std::toupper(f.at(0));
                break;
            case UPPER:
                t = toupper(t);
                f = toupper(t);
                break;
        }

        return (b) ? t : f;
    }

    // Comparison operator for two strings 
    inline bool operator> (string one, string two) {
        for (size_t t = 0; t < smallest(one.size(),two.size()); t++)
            if (one.at(t) > two.at(t))
                return true;
        
        // If one is longer than two, assume it is greater
        if (one.size() > two.size())
            return true;
        return false; // In all other cases, return false
    }

    // Additional operator for comparing strings
    inline bool operator>= (string one, string two) { return (one == two) ? true : one > two; }

    inline bool operator< (string one, string two) { return !(one > two); }

    inline bool operator<= (string one, string two) { return (one == two) ? true : !(one > two); }

    // Checks to see if given char is contained in string
    inline bool contains(string s, char c) {
        for (size_t t = 0; t < s.size(); t++)
            if (s.at(t) == c)
                return true;
        return false;
    }

    // Pauses the program until user presses [ENTER]. Prints a message, if supplied.
    inline void pause(string message = "Press [ENTER] to continue...") {
        cout << message;
        cin.ignore();
    }
};

#endif