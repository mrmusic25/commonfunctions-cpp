/* * * * * * * * * * * * * * * *
  allCommonFunctions.hpp - Includes all the common functions for easy importing
 * * * * * * * * * * * * * * * */
#ifndef ALLCOMMONFUNCTIONS_HPP
#define ALLCOMMONFUNCTIONS_HPP

#include "stringFunctions.hpp"
#include "vectorFunctions.hpp"
#include "numberFunctions.hpp"

#endif