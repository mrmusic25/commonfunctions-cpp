/* * * * * * * * * * * * * 
  numberFunctions.hpp - Common functions for numerical data (int, double, etc)
 * * * * * * * * * * * * */
#ifndef NUMBERFUNCTIONS_HPP
#define NUMBERFUNCTIONS_HPP

#include <iostream>
using namespace std;

namespace commonFunctions {
    // Returns the value of the greatest numerical value given
    template <class T>
    inline T greatest(T one, T two) { return (one >= two) ? one : two; }

    // Returns the smallest of the given values
    template <class T>
    inline T smallest(T one, T two) { return (one >= two) ? two : one; }
}

#endif