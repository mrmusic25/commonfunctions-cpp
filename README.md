# commonFunctions-cpp

A compilation of common functions I use in my C++ programs, similar to my original [commonFunctions.sh](https://gitlab.com/mrmusic25/linux-pref) script

# Intallation

Simply include the necessary headers for the desired object type(s) and you're good! (Note: See below for Logger.hpp instructions)

Optionally, you can also include the `allCommonFunctions.hpp` to automatically include all common functions in your program.

# Usage

Because it may overwrite/prefer my functions to cstdlib, I would NOT recommend using a `using namespace commonFunctions;` statement with the include (unless you will be using them extensively).

Preferably, use a `namespace cf = commonFunctions;` instead and just write `cf::function()` everywhere;

You could also just write a shortcut for extensively used functions.

Example: `using tu = commonFunctions::toupper();`

I highly recommend simply reading through the .hpp files, since most are short and easy to follow anyways.

Later, time permitting, I will go into detail here. Or perhaps make a wiki or UML.

# Logger

Originally written for my program [playlistConverter](https://gitlab.com/mrmusic25/playlistConverter), this is a simple header that allows logging messages to both the screen through use of cerr, and to a log file.

## Installation

Like before, just write `#include "Logger.hpp"` at the top of your project and you're good!

For this, I would recommed using `namespace Logger = commonFunctions::Logger`.

Note: This header needs Boost libraries. Specifically date_time, timer, filesystem, and thread (if you do not wish to install all of them). Any recent version should be fine.

## Usage

After declaring the log with `Logger log(filename)` (filling in `filename` with name of your choice, as a string), you only need to use the `log(message)` function.

You can specifiy the severity of the message using the following levels. By default, only `WARN` and above print to console.

0. `DBG`    - Debug messages that only print to console in verbose mode. Use this for in-depth details, since they won't print normally.
1. `INFO`   - Informational messages telling user what program is doing
2. `WARN`   - Problems that may cause unexpected behavior, but won't stop function/program.
3. `ERROR`  - A process or component has failed, but program will attempt to continue.
4. `FATAL`  - An unrecoverable error has occured and causing program to stop.
5. `NOTIFY` - Sends a notification to the user that will always display.

Example:

```c++
Logger log("myProgram.log");
log.log("An event happened at this time");
log.log("An error happened at this time!",ERROR);
```

Example using pointers (preferred)

```c++
Loggerr * log("otherProgram.log");
log->log("Program has encountered an error!",ERROR);
```

Pointer is preferred because you can easily pass the log to functions so they can use it as well.

`log.verbose()` will show INFO messages on console. `log.debug()` will enable printing and logging of debug messages. `log.defaultLogging()` restores the defaults for the log.

