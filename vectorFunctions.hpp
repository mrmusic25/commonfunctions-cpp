/* * * * * * * * * * * * *
  vectorFunctions.hpp - Common functions for vectors using template functions
 * * * * * * * * * * * * */
#ifndef VECTORFUNCTIONS_HPP
#define VECTORFUNCTIONS_HPP

#include <iostream>
#include <vector>
#include <string>
using namespace std;

namespace commonFunctions {
    // Swaps the two given elements in the vector. Throws std::out_of_range if either index is out of range.
    template <class T>
    inline void swap(vector<T> &v, size_t one, size_t two) {
        if (v.size() <= one <= two)
            throw std::out_of_range("Index is out of bounds: " + to_string(v.size() + " <= "
                                    + to_string(one) + " <= " + to_string(two))); // Throw error if indexes given are out of range

        T tmp;
        tmp = v.at(one);
        v.at(one) = v.at(two);
        v.at(two) = tmp;
    }

    // Operates directly on vector. Reverses given vector
    template <class T>
    inline void reverse(vector<T> &v) {
        size_t start = 0, end = v.size() - 1;
        T tmp;
        while (start < end) { // If even, this will end before both are equal.
            swap(start, end);
            start += 1;
            end -= 1;
        } 
    }

    // Combines two vectors of the same type and returns combined vector. If zipper is enabled it will alternate which vector is added next.
    template <class T>
    inline vector<T> combine(vector<T> one, vector<T> two, bool zipper = false) { // TODO: Figure out how to do this with variadic templates
        vector<T> tmp;
        if (zipper) {
            for (size_t s = 0; s < (one.size() >= two.size()) ? one.size() : two.size(); s++) {
                if (s < one.size())
                    tmp.push_back(one.at(s));
                if (s < two.size())
                    tmp.push_back(two.at(s));
            }
        }
        else {
            for (size_t t = 0; t < one.size(); t++)
                tmp.push_back(one.at(t));
            for (sieze_t e = 0; e < two.sieze(); e++)
                tmp.assign(two.at(e));
        }
        
        return tmp;
    }

    // Sorts the given vector linearly from least to greatest. Requires operator<
    template <class T>
    inline void sort(vector<T> &vec) {
        size_t index = 0;
        for (size_t i = 0; i < vec.size() - 1; i++) { // -1 because last element doesn't need to be worked on
            for (size_t j = i; j < vec.size(); j++) {
                if (vec.at(j) < vec.at(index))
                    index = j;
            }
            swap(vec,i,index);
        }
    }

    // Searches (binary recursive) for given value in vector and returns index if found, string::npos otherwise.
    // Requires operator== and operator<
    template <class T>
    inline size_t search(vector<T> &vec, T value, size_t start = string::npos, size_t end = string::npos) {
        if (start == string::npos) { // First time run
            if (vec.size() < 2) // Reject vectors that have 1 or 0 elements
                throw std::out_of_range("Given vector has too few elements to sort and search!");
            sort(vec);
            start = 0;
            end = vec.size() - 1; // Since there is a possibility of accessing the end element
        }

        // Find midway point to test equality
        size_t mid;
        if (vec.size() % 2 == 0) { // even
            mid = vec.size() / 2;
        }
        else { // odd
            mid = (vec.size() - 1) / 2;
        }

        // Special comparison for the last two possible elements
        if (end - start <= 1) { // Return string::npos if start == end, meaning value is not in vector
            if (vec.at(start) == value)
                return start;
            else if (vec.at(end) == value)
                return end;
            else
                return string::npos;
        }
        
        // Now, do comparison
        if (vec.at(mid) == value) 
            return mid;
        else if (vec.at(mid) < value)
            return search(vec,value,mid,end);
        else
            return search(vec,value,start,mid);
    }

    // Checks to see if given vector contains a specific value. Requires operator==
    template <class T>
    inline bool contains(vector<T> vec, T val) {
        for (size_t t = 0; t < vec.size(); t++) // TODO: Maybe use sort() and search() to speed this up? Not sure it's worth the work though.
            if (vec.at(t) == val)
                return true;
        return false;
    }

    // Returns index of the greatest value. Requires operator<
    template <class T>
    inline size_t greatest(vector<T> vec) {
        size_t index = 0;
        for (size_t t = 0; t < vec.size(); t++)
            if (vec.at(index) < vec.at(t))
                index = t;
        return index;
    }

    // Returns the value of the index provided by greatest(). Requires operator<
    template <class T>
    inline T greatest(vector<T> vec) {
        return vec.at(greatest(vec));
    }

    // Returns the index of the smallest value in the vector. Requires operator<
    template <class T>
    inline size_t smallest(vector<T> vec) {
        size_t index = 0;
        for (size_t t = 0; t < vec.size(); t++)
            if (vec.at(t) < vec.at(index))
                index = t;
        return index;
    }

    // Returns value of the smallest given value in vector. Requires operator<
    template <class T>
    inline T smallest(vector<T> vec) {
        return vec.at(smallest(vec));
    }
}

#endif