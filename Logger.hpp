/* * * * * * * * * * 
  Logger.hpp - Create and maintain a log of actions a program has taken, for user and debug info
 * * * * * * * * * */
#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <iostream>
#include <string>
#include <boost/timer/timer.hpp> // Log times
#include "boost/date_time/posix_time/posix_time.hpp" // Write current date to log for reference
#include <boost/filesystem.hpp>
#include <boost/thread.hpp> // Needed for sleep()
using namespace std;
using namespace boost::filesystem;
using namespace boost::timer;

namespace commonFunctions {
    // enum used for deciding where output is sent
    // Messages will always be sent to the log, except for DBG level. Must be manually enabled.
    // Message priority can be specified, but by default only ERROR and FATAL will be sent to stderr
    enum Level { 
                FATAL,   // For issues that cause the program to close 
                ERROR,   // For issues that result in a failed function (i.e. failed try-catch)
                WARN,    // For when the program CAN continue, but later issues might arise
                INFO,    // General logging info
                DBG,     // More detailed infor for debugging
                NOTIFY,  // Special mode that always notifies user
               };

    class Logger {
        private:
            /* * * * * * * 
              Class Data
             * * * * * * */
            string filename;                       // Location logFile the log file itself
            Level errLevel = ERROR,                // Max level that will be output to console
                logLevel = INFO,                 // Max level written to log file
                defLevel = INFO;                 // Default level given to messages when not specified
            bool isInitialized = false;            // Tells log whether or not to output init info to beginning logFile log, should only be run once
            cpu_timer exeTime;                     // Boost timer for precise logging time
            bool writeLock = false,                // Used to tell whether log can be written to
                notifyLock = false;               // Similarly, a flag to tell whether info can be output yet
            int waitTime = 150;                    // Time, in milliseconds, to wait between writes and log outputs. Used for threading.

            /* * * * * * * * * * 
              Private Functions
             * * * * * * * * * * */
            void prepareFile() {
                // Sanity check
                if (isInitialized == true)
                    return;

                // Start timer
                exeTime = cpu_timer();
                exeTime.start();

                // Write the initial log time in wall clock format
                boost::posix_time::ptime timeLocal = boost::posix_time::second_clock::local_time();
                write("*** Log started at " + to_simple_string(timeLocal) + " ***", INFO);
                
                // Make sure function isn't run again (by this instance)
                isInitialized = true;
            }

            void write(string message) {
                write(message,defLevel);           
            }

            void write(string message, Level l) {
                while (writeLock)
                    boost::this_thread::sleep(boost::posix_time::milliseconds(waitTime));
                writeLock = true;
                boost::filesystem::ofstream f(filename,ios_base::out | ios_base::app);
                f << "[" << exeTime.format(6, "%w") << "] " << getLevelString(l) << ": " << message << endl;
                f.close();         
                writeLock = false;       
            }

            void notify(string message) { 
                notify(message,defLevel);
            }
            
            void notify(string message, Level l) { 
                while (notifyLock)
                    boost::this_thread::sleep(boost::posix_time::milliseconds(waitTime));
                notifyLock = true;
                stringstream s;
                s << "[" << exeTime.format(6, "%w") << "] " << getLevelString(l) << ": " << message << endl;
                cerr << s.str();
                notifyLock = false; 
            }

        public:
            /* * * * * * 
              Constructors
             * * * * * * */
            Logger() {
                filename = "Default.log";
                isInitialized = true;
            }

            Logger(string s) {
                filename = s;
                prepareFile();
            }

            ~Logger() {
                boost::posix_time::ptime timeLocal = boost::posix_time::second_clock::local_time();
                write("*** Log closed at " + to_simple_string(timeLocal) + " ***", INFO);
            }

            /* Note: No copy constructor!
            Use case: Initialize the log once, then have classes accept pointer to the initialized log so they can work with it directly
            TODO: Find a better way to implement this, maybe switch class to static memebers?
            */

            /* * * * * * 
              Modifiers
             * * * * * * */
            void setLogLevel(Level l)     { logLevel = l; }
            void setOutputLevel(Level l)  { errLevel = l; }
            void setDefaultLevel(Level l) { defLevel = l; }
            void setFilename(string s) { filename = s; }
            void debugMode() { // Sets options for debugging mode, by setting everything to DBG mode (literally every message will be printed and logged)
                logLevel = DBG;
                errLevel = DBG;
                defLevel = DBG;
            }

            /* * * * * * 
              Accessors
             * * * * * * */
            const string     getFilename()     { return filename; } // Note: See above for descriptions
            const Level      getDefaultLevel() { return defLevel; }
            const Level      getErrorLevel()   { return errLevel; }
            const Level      getLogLevel()     { return logLevel; }
            cpu_timer      * getTimer()        { return &exeTime; }

            /* * * * * * * * * * *
              Member Functions
             * * * * * * * * * * */
            
            // Turns on verbose mode
            void verbose() {
                errLevel = INFO;
            }

            // Turns on debug logging. Only logs to file if enableLogging is enabled
            void debug(bool enableLogging = false) {
                errLevel = DBG;
                if (enableLogging)
                    logLevel = DBG;
            }

            // Restores default log settings
            void defaultLogging() {
                errLevel = WARN;
                logLevel = INFO;
            }

            // Returns a string of the given Level
            string getLevelString(Level l) {
                string levels[] = { "FATAL", "ERROR", "WARN", "INFO", "DBG", "NOTIFY" };
                return levels[(int)l];
            }

            // Logs message using the set default level
            void log(string message) {
                write(message);
                if (defLevel <= errLevel) 
                    notify(message); // By default shouldn't send message
            }

            // Logs message with the given log level
            void log(string message, Level l) {
                if (l <= logLevel) 
                    write(message,l);

                if (l <= errLevel)
                    notify(message,l);
            }
    };
};
#endif